CXX1=g++
CXX2=clang++
CXXFLAGS=-std=c++11 -O3 -fomit-frame-pointer

all : except noexcept asm

asm : except.cpp noexcept.cpp
	$(CXX1) $(CXXFLAGS) -S -o except-gcc.S except.cpp
	$(CXX2) $(CXXFLAGS) -S -o except-clang.S except.cpp
	$(CXX1) $(CXXFLAGS) -S -o noexcept-gcc.S noexcept.cpp
	$(CXX2) $(CXXFLAGS) -S -o noexcept-clang.S noexcept.cpp

except : except.cpp 
	$(CXX1) $(CXXFLAGS) -o except-gcc except.cpp
	$(CXX2) $(CXXFLAGS) -o except-clang except.cpp

noexcept : noexcept.cpp 
	$(CXX1) $(CXXFLAGS) -o noexcept-gcc noexcept.cpp
	$(CXX2) $(CXXFLAGS) -o noexcept-clang noexcept.cpp

clean :
	rm except-gcc except-clang noexcept-gcc noexcept-clang
