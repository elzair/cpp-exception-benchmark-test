#include <iostream>
#include <cstdint>
#include <ctime>
#include <vector>
#include <list>
#include <cmath>

#define NUMTRIALS 1000

long int g_i = 0;

bool noException() {
  g_i += 1;

  if (g_i < 0) {
    return false;
  }
  return true;
}

int main(void) {
  int i, j;
  uint64_t times[NUMTRIALS];
  uint64_t sum = 0, oldsum = 0;

  // Perform trials
  
  for (i = 0; i < NUMTRIALS; i++) {
    struct timespec start, stop;

    clock_gettime(CLOCK_REALTIME, &start);

    for (j = 0; j < 100000000; j++) {
      bool val = noException();

      if (val == false) {
        return -1;
      }
    }

    clock_gettime(CLOCK_REALTIME, &stop);

    uint64_t start_64 = (((uint64_t) start.tv_sec) * 1000000000UL) + (uint64_t) start.tv_nsec;
    uint64_t stop_64  = (((uint64_t) stop.tv_sec)  * 1000000000UL) + (uint64_t) stop.tv_nsec;
    times[i]          = stop_64 - start_64;
    oldsum            = sum;
    sum              += times[i];

    if (sum < oldsum) {
      std::cerr << "Overflow on Mean calculation!" << std::endl;
      return -2;
    }
  }

  // Compute average

  uint64_t average = sum / (uint64_t)NUMTRIALS;

  // Compute standard deviation

  uint64_t sqrdiff[NUMTRIALS];
  uint64_t sqrdiffsum = 0, oldsqrdiffsum = 0;

  for (i = 0; i < NUMTRIALS; i++) {
    uint64_t diff = (times[i] > average) ? (times[i] - average) : (average - times[i]);
    sqrdiff[i]    = diff * diff;
    oldsqrdiffsum = sqrdiffsum;
    sqrdiffsum   += sqrdiff[i];

    if (sqrdiffsum < oldsqrdiffsum) {
      std::cerr << "Overflow on Standard Deviation calculation!" << std::endl;
      return -3;
    }
  }

  uint64_t    variance = sqrdiffsum / (uint64_t)NUMTRIALS;
  long double stddev   = sqrt((long double)variance);

  std::cout << "Average: "            << average << " nanoseconds" << std::endl;
  std::cout << "Standard Deviation: " << stddev  << " nanoseconds" << std::endl;

  return 0;
}

